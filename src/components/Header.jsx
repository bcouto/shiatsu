import React, { Component } from "react";
import LoginModal from "./home/LoginModal";
import {
  Collapse,
  Navbar,
  NavbarToggler,
  NavbarBrand,
  Nav,
  NavItem
} from "reactstrap";
import { Link } from "react-router-dom";

class Header extends Component {
  constructor(props) {
    super(props);

    this.state = {
      isOpen: false,
      modal: false
    };
  }
  navToggle = () => {
    const isOpenState = this.state.isOpen;
    this.setState({
      isOpen: !isOpenState
    });
  };

  modalToggle = () => {
    const modalState = this.state.modal;
    this.setState({
      modal: !modalState
    });
  };

  render() {
    return (
      <div className="header">
        <Navbar light expand="md">
          <Link to="/" style={{ textDecoration: "none", color: "#212529" }}>
            <NavbarBrand>
              <img
                className="logo-image"
                src="https://vignette.wikia.nocookie.net/logopedia/images/8/8e/B2W_logo.png/revision/latest?cb=20131119171859"
                alt=""
              />
            </NavbarBrand>
            <span className="header-title">Shiatsu</span>
          </Link>
          <NavbarToggler onClick={this.navToggle} />
          <Collapse isOpen={this.state.isOpen} navbar>
            <Nav className="ml-auto" navbar>
              <NavItem>
                <span className="header-item" onClick={this.modalToggle}>
                  Login
                </span>
              </NavItem>
              <NavItem>
                <span className="header-item">O que é Shiatsu?</span>
              </NavItem>
            </Nav>
          </Collapse>
        </Navbar>
        <LoginModal toggle={this.modalToggle} modal={this.state.modal} />
      </div>
    );
  }
}

export default Header;
