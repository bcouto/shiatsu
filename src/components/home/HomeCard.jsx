import React from "react";
import {
  Card,
  CardImg,
  CardBody,
  CardTitle,
  CardText,
  Button
} from "reactstrap";
import LoginModal from "./LoginModal";

class HomeCard extends React.Component {
  state = {
    hasSessions: true,
    modal: false
  };

  render() {
    return (
      <Card className="home-card" body>
        {this.showCard()}
      </Card>
    );
  }

  showCard = () => {
    const card = this.state.hasSessions
      ? this.openedSessionsCard()
      : this.closedSessionsCard();
    return card;
  };

  openedSessionsCard = () => (
    <div>
      <CardImg
        top
        width="100%"
        src="https://www.spaaura.com/wp-content/uploads/2016/09/Cute-bulldog-gets-a-massage-768x512.jpg"
        alt=""
      />
      <CardBody className="text-center">
        <CardTitle>Inscrições abertas!</CardTitle>
        <CardText>Faça o login e inscreva-se no horário desejado</CardText>
        <Button color="success" size="lg" onClick={this.toggle}>
          Login
        </Button>
      </CardBody>
      <LoginModal toggle={this.toggle} modal={this.state.modal} />
    </div>
  );

  closedSessionsCard = () => (
    <div>
      <CardImg
        top
        width="100%"
        src="https://sadanduseless.b-cdn.net/wp-content/uploads/2015/05/sad-pug1.jpg"
        alt=""
      />
      <CardBody className="text-center">
        <CardTitle>Inscrições encerradas</CardTitle>
        <CardText>Não há sessões abertas no momento :(</CardText>
      </CardBody>
    </div>
  );

  toggle = () => {
    const modalState = this.state.modal;
    this.setState({
      modal: !modalState
    });
  };
}

export default HomeCard;
