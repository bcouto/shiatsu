import React from "react";
import { Container, Row, Col, Button, Card, CardTitle } from "reactstrap";
import mock from "../../mock.json";
import userImgGreen from "../../assets/images/user-green.svg";
import userImgYellow from "../../assets/images/user-yellow.svg";
import userImgRed from "../../assets/images/user-red.svg";
import TimeTableModal from "./TimeTableModal";

class Home extends React.Component {
  state = {
    modal: false,
    sessions: mock.sessions,
    totalUsers: 0,
    avgUsers: 0,
    afternoonSessions: {},
    morningSessions: {},
    selectedSession: ""
  };

  componentWillMount() {
    this.setSessions();
  }

  render() {
    return (
      <div className="time-table-wrapper">
        <Container className="morning-container">
          <Card className="hours-card">
            <CardTitle style={{ textAlign: "center" }}>Manhã</CardTitle>
            <Row>{this.renderHours(this.state.morningSessions)}</Row>
          </Card>
        </Container>
        <Container className="afternoon-container">
          <Card className="hours-card">
            <CardTitle style={{ textAlign: "center" }}>Tarde</CardTitle>
            <Row>{this.renderHours(this.state.afternoonSessions)}</Row>
          </Card>
        </Container>
        <TimeTableModal
          session={this.state.selectedSession}
          toggle={this.toggle}
          modal={this.state.modal}
        />
      </div>
    );
  }

  setSessions = () => {
    let sessionsUsers = 0;

    this.state.sessions.map(sessison => {
      return (sessionsUsers += sessison.usersQueue.length);
    });

    const afternoonHours = this.state.sessions
      .filter(session => parseInt(session.hour) > 13 && session.open === true)
      .map(session => {
        return {
          hour: session.hour,
          id: session._id,
          usersQueue: session.usersQueue.length
        };
      });

    const morningHours = this.state.sessions
      .filter(session => parseInt(session.hour) <= 13 && session.open === true)
      .map(session => {
        return {
          hour: session.hour,
          id: session._id,
          usersQueue: session.usersQueue.length
        };
      });

    const avg = Math.floor(sessionsUsers / this.state.sessions.length);

    this.setState({
      morningSessions: morningHours,
      afternoonSessions: afternoonHours,
      totalUsers: sessionsUsers,
      avgUsers: avg
    });
  };

  toggle = () => {
    const modalState = this.state.modal;
    this.setState({
      modal: !modalState
    });
  };

  clickHandler = session => {
    this.setState({
      selectedSession: session
    });
    this.toggle();
  };

  renderHours = sessions => {
    return sessions.map(session => {
      let btnColor, textColor, imgColor;

      if (session.usersQueue < this.state.avgUsers - 1) {
        btnColor = "success";
        textColor = "#28a745";
        imgColor = userImgGreen;
      } else if (session.usersQueue > this.state.avgUsers + 1) {
        btnColor = "danger";
        textColor = "#dc3545";
        imgColor = userImgRed;
      } else {
        btnColor = "warning";
        textColor = "#ffc107";
        imgColor = userImgYellow;
      }

      return (
        <Col xs="auto">
          <div className="col-wrapper">
            <Button
              size="lg"
              className="hour-button"
              color={btnColor}
              onClick={() => this.clickHandler(session)}
              outline
            >
              {session.hour}
            </Button>
            <div className="people-wrapper" style={{ color: textColor }}>
              <span className="people-counter">{session.usersQueue}</span>
              <img className="user-img" src={imgColor} />
            </div>
          </div>
        </Col>
      );
    });
  };
}

export default Home;
