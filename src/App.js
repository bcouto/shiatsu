import React, { Component } from "react";
import "./App.css";
import Header from "./components/Header";
import TimeTable from "./components/time-table/TimeTable";
import {
  BrowserRouter as Router,
  Route,
  Redirect,
  Switch
} from "react-router-dom";
import HomeCard from "./components/home/HomeCard";
import CreateSession from "./components/admin/CreateSession";

class App extends Component {
  render() {
    return (
      <Router>
        <div>
          <Header />
          <Switch>
            <Route exact path="/" component={HomeCard} />
            <Route exact path="/timetable" component={TimeTable} />
            <Route exact path="/create" component={CreateSession} />
            <Redirect to="/" />
          </Switch>
        </div>
      </Router>
    );
  }
}

export default App;
