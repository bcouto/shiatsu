import React from "react";
import { Button, Modal, ModalHeader, ModalBody, ModalFooter } from "reactstrap";

class TimeTableModal extends React.Component {
  state = {
    modalConfirmed: false
  };

  handleSubmit = () => {
    const toggle = this.props.toggle;
    this.toggleConfirmed();
    toggle();
  };

  toggleConfirmed = () => {
    const modalState = this.state.modalConfirmed;
    this.setState({
      modalConfirmed: !modalState
    });
  };

  confirmedModal = () => {
    const session = this.props.session;
    return (
      <Modal isOpen={this.state.modalConfirmed} toggle={this.toggleConfirmed}>
        <ModalHeader toggle={this.toggleConfirmed}>
          Inscrição confirmada
        </ModalHeader>
        <ModalBody>
          <p>Sua inscrição para a sessão das {session.hour} foi realizada.</p>
          <p>
            Você receberá um e-mail após o sorteio que dirá se você foi um dos
            selecionados.
          </p>
          <p>Boa sorte!</p>
        </ModalBody>
      </Modal>
    );
  };

  confirmationModal = () => {
    const { session, toggle, modal } = this.props;
    return (
      <Modal isOpen={modal} toggle={toggle}>
        <ModalHeader toggle={toggle}>Confirmação</ModalHeader>
        <ModalBody>
          Gostaria de se inscrever para a sessão das {session.hour}?
        </ModalBody>
        <ModalFooter>
          <Button color="success" onClick={() => this.handleSubmit()}>
            Sim
          </Button>
          <Button color="danger" onClick={() => toggle()}>
            Não
          </Button>
        </ModalFooter>
      </Modal>
    );
  };

  render() {
    return (
      <React.Fragment>
        {this.confirmationModal()}
        {this.confirmedModal()}
      </React.Fragment>
    );
  }
}

export default TimeTableModal;
