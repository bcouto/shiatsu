import React from "react";
import {
  Button,
  Modal,
  ModalHeader,
  ModalBody,
  ModalFooter,
  Form,
  FormGroup,
  Label,
  Input
} from "reactstrap";
import { BrowserRouter as Route, Redirect } from "react-router-dom";
import { login } from "../../scripts/login";

class LoginModal extends React.Component {
  state = {
    user: {
      email: "",
      password: ""
    },
    redirect: false
  };

  render() {
    const { toggle, modal } = this.props;

    if (this.state.redirect) {
      return (
        <Redirect
          to={{
            pathname: this.state.redirectTo,
            state: { user: this.state.response }
          }}
        />
      );
    }

    return (
      <Modal isOpen={modal} toggle={toggle}>
        <ModalHeader toggle={toggle}>Login</ModalHeader>
        <ModalBody>
          <Form>
            <FormGroup>
              <Label for="exampleEmail">Email</Label>
              <Input type="email" name="email" onChange={this.handleChange} />
            </FormGroup>
            <FormGroup>
              <Label for="examplePassword">Senha</Label>
              <Input
                type="password"
                name="password"
                onChange={this.handleChange}
              />
            </FormGroup>
          </Form>
        </ModalBody>
        <ModalFooter>
          <Button color="success" onClick={() => this.handleSubmit()}>
            Login
          </Button>
        </ModalFooter>
      </Modal>
    );
  }

  handleSubmit = async () => {
    const { toggle } = this.props;

    try {
      const response = await login(this.state.user);
      const redirectEndpoint = response.admin ? "/create" : "/timetable";
      this.setState({
        response: response,
        redirectTo: redirectEndpoint,
        redirect: true
      });
      toggle();
    } catch (error) {
      console.error(error);
    }
  };

  handleChange = evt => {
    const userState = this.state.user;
    this.setState({
      user: {
        ...userState,
        [evt.target.name]: evt.target.value
      }
    });
  };
}

export default LoginModal;
